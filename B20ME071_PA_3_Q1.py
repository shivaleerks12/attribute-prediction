import os
import torch
from torchvision.datasets import ImageFolder
from torchvision.transforms import transforms
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, models
import zipfile
from tqdm import tqdm
import pandas as pd
import numpy as np
import cv2
from torchvision.models import resnet18
import torch.nn as nn
import torch.optim as optim

df_attr = pd.read_csv('/iitjhome/b20me071/scratch/data/b20me071/dl3/CelebA_Dataset/list_attr_celeba.txt', delim_whitespace=True, header=1)

df = df_attr[['Male', 'Smiling', 'Eyeglasses', 'Wearing_Hat', 'Pale_Skin', 'Blond_Hair', 'Wavy_Hair', 'Mustache']]
class CelebADataset(Dataset):
    def __init__(self, root_dir, df, transform=None):
        self.root_dir = root_dir
        self.df = df
        self.transform = transform
        
    def __len__(self):
        return len(self.df)
    
    def __getitem__(self, idx):
        img_name = os.path.join(self.root_dir, f'{idx+1:06d}.jpg')
        img = cv2.imread(img_name)
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
        
        label = torch.from_numpy(self.df.iloc[idx].values.astype(np.float32))
        
        # replace -1 with 0
        label[label==-1] = 0
        
        # label = torch.from_numpy(label)

        if self.transform:
            img = self.transform(img)
        
        return img, label
transform = transforms.Compose([
    transforms.ToPILImage(),
    transforms.RandomHorizontalFlip(),
    transforms.RandomCrop((128, 128)),
    transforms.Resize((64, 64)),
    transforms.ToTensor(),
    transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
])

train_dataset = CelebADataset('/iitjhome/b20me071/scratch/data/b20me071/dl3/CelebA_Dataset/img_align_celeba', df.iloc[:80000], transform=transform)
test_dataset = CelebADataset('/iitjhome/b20me071/scratch/data/b20me071/dl3/CelebA_Dataset/img_align_celeba', df.iloc[80000:160000], transform=transform)

train_loader = DataLoader(train_dataset, batch_size=32, shuffle=True, num_workers=4)
test_loader = DataLoader(test_dataset, batch_size=32, shuffle=False, num_workers=4)

# Define the eight attributes to predict
attributes = ['Male', 'Smiling', 'Eyeglasses', 'Wearing hat', 'Pale skin', 'Blond hair', 'Wavy hair', 'Mustache']

# Load the ResNet18 model without pre-trained weights
backbone = models.resnet18(pretrained=False)

# Load the pre-trained weights
state_dict = torch.load("/iitjhome/b20me071/scratch/data/b20me071/dl3/resnet18.pth")

# Load the weights into the model
backbone.load_state_dict(state_dict)
backbone.fc = nn.Linear(512, 512)
# Define the shared layers
shared_layers = nn.Sequential(
    nn.Linear(512, 256),
    nn.ReLU(inplace=True),
    nn.Dropout(p=0.5),
    nn.Linear(256, 128),
    nn.ReLU(inplace=True),
    nn.Dropout(p=0.5)
)

# Define the task-specific output layers
num_attributes = 8
attribute_outputs = [nn.Sequential(
    nn.Linear(128, 64),
    nn.ReLU(inplace=True),
    nn.Linear(64, 1),
    nn.Sigmoid()
) for _ in range(num_attributes)]
# Combine the backbone, shared layers, and output layers
model = nn.Sequential(
    backbone,
    shared_layers,
    *attribute_outputs
)

# Define the optimizer, loss function, and learning rate scheduler
optimizer = optim.SGD(model.parameters(), lr=0.01, momentum=0.9, weight_decay=1e-4)
loss_fn = nn.BCELoss()
scheduler = optim.lr_scheduler.StepLR(optimizer, step_size=10, gamma=0.1)

from tqdm import tqdm
print("training starts")
print_loss_freq = 100
loss_counter = 0

for epoch in range(10):
    # Set the model to train mode
    model.train()

    # Iterate over the training set
    pbar_train = tqdm(train_loader, desc=f"Training Epoch {epoch + 1}", leave=False)
    for images, labels in pbar_train:
        # Zero the gradients
        optimizer.zero_grad()

        # Forward pass
        shared_features = model[0](images)
        shared_features = shared_layers(shared_features)
        outputs = [attribute_outputs[i](shared_features) for i in range(num_attributes)]

        # Compute the loss
        loss = sum([loss_fn(outputs[i], labels[:, i].unsqueeze(1)) for i in range(num_attributes)])

        # Backward pass
        loss.backward()
        optimizer.step()

        # Update progress bar
        pbar_train.set_postfix({"loss": f"{loss.item():.4f}"})

        # Print loss after every few iterations
        loss_counter += 1
        if loss_counter % print_loss_freq == 0:
            print(f"Loss after {loss_counter} iterations: {loss.item()}")

    # Adjust the learning rate
    scheduler.step()

torch.save(model.state_dict(), '/iitjhome/b20me071/scratch/data/b20me071/dl3/model.pth') #define the path to save model

    
with torch.no_grad():
    correct = [0] * len(attributes)
    total = [0] * len(attributes)
    for inputs, targets in test_loader:
        outputs =  model[0](inputs)
        for i in range(len(attributes)):
            predicted = (outputs[:, i] > 0.5).int()
            correct[i] += (predicted == targets[:, i].int()).sum().item()
            total[i] += targets[:, i].size(0)
    
    # Print task-wise accuracy
    for i in range(len(attributes)):
        print('%s accuracy: %.2f%%' % (attributes[i], 100 * correct[i] / total[i]))
    
    # Print overall accuracy
    overall_correct = sum(correct)
    overall_total = sum(total)
    overall_accuracy = 100 * overall_correct / overall_total
    print('Overall accuracy: %.2f%%' % overall_accuracy)